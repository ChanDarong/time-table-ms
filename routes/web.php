<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\ClassSubjectController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Models\ClassSubjectModel;
use App\Models\Grade;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [AuthController::class, 'login']);
Route::post('login', [AuthController::class, 'AuthLogin']);
Route::get('logout', [AuthController::class, 'logout']);
Route::get('forgot-password', [AuthController::class, 'forgotpassword']);
Route::post('forgot-password', [AuthController::class, 'PostForgotPassword']);
Route::get('reset/{token}', [AuthController::class, 'reset']);
Route::post('reset/{token}', [AuthController::class, 'PostReset']);

// Route::get('admin/dashboard', function () {
//     return view('admin.dashboard');
// })->name('admin.dashboard');



Route::group(['middleware' => 'admin'], function () {

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('admin.dashboard');

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        // Route::resource('grade', GradeController::class);
        Route::get('/dashboard', [DashboardController::class, 'dashboard']);
        Route::get('admin/list', [AdminController::class, 'list'])->name('admin.list');
        Route::get('admin/add', [AdminController::class, 'add']);
        Route::post('admin/add', [AdminController::class, 'insert']);
        Route::get('admin/edit/{id}', [AdminController::class, 'edit']);
        Route::post('admin/edit/{id}', [AdminController::class, 'update']);
        Route::get('admin/delete/{id}', [AdminController::class, 'delete']);



        //class
        Route::get('class/list', [ClassController::class, 'list']);
        Route::get('class/add', [ClassController::class, 'add']);
        Route::post('class/add', [ClassController::class, 'insert']);
        Route::get('class/edit/{id}', [ClassController::class, 'edit']);
        Route::post('class/edit/{id}', [ClassController::class, 'update']);
        Route::get('class/delete/{id}', [ClassController::class, 'delete']);

        //Subject
        Route::get('subject/list', [SubjectController::class, 'list']);
        Route::get('subject/add', [SubjectController::class, 'add']);
        Route::post('subject/add', [SubjectController::class, 'insert']);
        Route::get('subject/edit/{id}', [SubjectController::class, 'edit']);
        Route::post('subject/edit/{id}', [SubjectController::class, 'update']);
        Route::get('subject/delete/{id}', [SubjectController::class, 'delete']);


        ///assign_subject
        Route::get('assign_subject/list', [ClassSubjectController::class, 'list']);
        Route::get('assign_subject/add', [ClassSubjectController::class, 'add']);
        Route::post('assign_subject/add', [ClassSubjectController::class, 'insert']);
        Route::get('assign_subject/edit/{id}', [ClassSubjectController::class, 'edit']);
        Route::post('assign_subject/edit/{id}', [ClassSubjectController::class, 'update']);

        Route::get('assign_subject/edit_single/{id}', [ClassSubjectController::class, 'edit_single']);
        Route::post('assign_subject/edit_single/{id}', [ClassSubjectController::class, 'update_single']);
        Route::get('assign_subject/delete/{id}', [ClassSubjectController::class, 'delete']);


        //change_password
        Route::get('change_password', [UserController::class, 'change_password']);
        Route::post('change_password', [UserController::class, 'update_change_password']);

        //Student
        Route::get('student/list', [StudentController::class, 'list']);
        Route::get('student/add', [StudentController::class, 'add']);
        Route::post('student/add', [StudentController::class, 'insert']);



        //Teacher
        Route::get('teacher/list', [TeacherController::class, 'list']);
        Route::get('teacher/add', [TeacherController::class, 'add']);
        Route::post('teacher/add', [TeacherController::class, 'insert']);
        Route::get('teacher/edit/{id}', [TeacherController::class, 'edit']);
        Route::post('teacher/edit/{id}', [TeacherController::class, 'update']);
        Route::get('teacher/delete/{id}', [TeacherController::class, 'delete']);
        Route::get('teacher/detail/{id}', [TeacherController::class, 'detail']);

        // Grade
        Route::resource('grade', GradeController::class);

    });

});






Route::group(['middleware' => 'teacher'], function () {
    Route::get('teacher/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('teacher/change_password', [UserController::class, 'change_password']);
    Route::post('teacher/change_password', [UserController::class, 'update_change_password']);
});

Route::group(['middleware' => 'student'], function () {
    Route::get('student/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('student/change_password', [UserController::class, 'change_password']);
    Route::post('student/change_password', [UserController::class, 'update_change_password']);
});

Route::group(['middleware' => 'parent'], function () {
    Route::get('parent/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('parent/change_password', [UserController::class, 'change_password']);
    Route::post('parent/change_password', [UserController::class, 'update_change_password']);
});
