<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function Termwind\render;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $grades = Grade::paginate();

        return view('admin.grade.index', compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.grade.partials._create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // return $request->all();
        // dd($request->all());

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'success' => false,
            ]);
        }

        Grade::create(['name' => $request->name]);

        $grades = Grade::paginate();

        $view = view('admin.grade.partials._table', compact('grades'))->render();

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Grade $grade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Grade $grade)
    {
        // $grade = Grade::findOrFail($id)
        // dd($grade);

        return view('admin.grade.partials._edit', compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Grade $grade)
    {
        $grade->name = $request->name;
        $grade->save();

        $grades = Grade::paginate();
        $view = view('admin.grade.partials._table', compact('grades'))->render();

        return response()->json([
            'view' => $view,
            'data' => $request->all(),
        ]);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Grade $grade)
    {
        $grade->delete();

        $grades = Grade::paginate();
        $view = view('admin.grade.partials._table', compact('grades'))->render();

        return response()->json([
            'view' => $view,
        ]);
    }
}
