<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class ClassRoom extends Model
{
    use HasFactory;
    // protected $table = 'Class';

    public function grade () {
        return $this->belongsTo(Grade::class);
    }

    static public function getRecord(){
        $return = ClassRoom::select('class_rooms.*','users.name as created_by_name')
        ->join('users','users.id','class_rooms.created_by');

        if(!empty(Request::get('name'))){
            $return = $return->where('class_rooms.name','like','%' .Request::get('name').'%');
        }
        if(!empty(Request::get('date'))){
            $return = $return->whereDate('class_rooms.created_at','=',Request::get('date'));
        }

        $return=$return->where('class_rooms.is_delete','=',0)
        ->orderBy('class_rooms.id', 'desc')
        ->paginate(20);
        return $return ;
    }
    static public function getSingle($id){
        return self::find($id);
    }


    static public function getClass()
    {
        $return = ClassRoom::select('class_rooms.*')
        ->join('users','users.id','class_rooms.created_by')
        ->where('class_rooms.is_delete', '=', 0)
        ->where('class_rooms.status','=',0)
        ->orderBy('class_rooms.name','asc')
        ->get();
        return $return;


    }
}
