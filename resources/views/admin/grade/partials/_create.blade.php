<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Add Grade') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.grade.store') }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="name">{{ __('Name') }}</label>
                    <input type="number" name="name" min="1" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({

            type: "post",
            url: `{{ route('admin.grade.store') }}`,
            data: data,
            // dataType: "json",
            success: function (response) {
                // console.log(response);
                if (response.success == false) {
                    alert('error');
                } else {
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                }

                $('.table-wrapper').replaceWith(response.view);
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-right',
                    // iconColor: 'white',
                    customClass: {
                        popup: 'colored-toast'
                    },
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true
                });
                Toast.fire({
                    icon: 'success',
                    title: 'Success'
                });

            }
        });
    });
</script>
