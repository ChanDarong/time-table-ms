<div class="card-body p-0 table-wrapper">
    <table class="table table-striped">
        <thead>
            <tr>
                <th >#</th>
                <th>Name</th>
                {{-- <th>Created by</th> --}}
                <th>Create date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($grades as $grade)
                <tr>
                    <td>{{$grade->id}}</td>
                    <td>{{$grade->name}}</td>
                    <td>{{ $grade->created_at->format('d M Y h:i A') }}</td>
                    <td>
                        <a href="#" data-href="{{ route('admin.grade.edit', $grade->id) }}" data-container=".modal_form" class="btn btn-primary btn-sm btn-edit">{{ __('Edit') }}</a>

                        <form action="{{ route('admin.grade.destroy', $grade->id) }}" class="d-inline-block form-delete-{{ $grade->id }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" data-id="{{ $grade->id }}" data-href="{{ route('admin.grade.destroy', $grade->id) }}" class="btn btn-danger btn-sm btn-delete">{{ __('Delete') }}</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="padding: 10px; float: right;">
        {{-- {!! $getRecord->appends(Illuminate\Support\Facades\Request::except('page'))->links()!!} --}}
        {{ $grades->links() }}
    </div>
</div>
